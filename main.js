const Color = require('./lib/Color')
const Discord = require('discord.js')
const Faction = require('./lib/Faction')
const Freight = require('./lib/Freight')
const Government = require('./lib/Government')
const Name = require('./lib/Name')
const Passenger = require('./lib/Passenger')
const Passengers = require('./lib/passengers')
const Price = require('./lib/Price')
const Roll = require('./lib/Roll')
const Subsector = require('./lib/Subsector')
const Trade = require('./lib/trade')
const World = require('./lib/World')

function output (channel, data) {
  const promise = channel.send(
    'content' in data[0] ? data[0].content : '',
    data[0]
  )

  if (data.length > 1) {
    promise.then(() => {
      output(channel, data.slice(1))
    })
  }

  return promise
}

function printHelp () {
  return [
    {
      embed: {
        color: Color.Blue,
        title: 'Basic Commands',
        fields: [
          {
            name: '~black-market [origin] [destination]',
            value: 'Roll black market goods between the two worlds.'
          },
          {
            name: '~buy [price] [effect]',
            value: "Modify a purchasing price based on a roll's effect."
          },
          {
            name: '~faction',
            value: 'Geneate a random faction description.'
          },
          {
            name: '~freight [world] [effect]',
            value: 'Rolls freight cargo available on the specified world.'
          },
          {
            name: '~gamble [bet] [effect]',
            value: 'Rolls gambling result based on a Gambling roll effect.'
          },
          {
            name: '~government [0–F]',
            value: 'Print out summary of government rating.'
          },
          {
            name: '~name',
            value: 'Generate a random name.'
          },
          {
            name: '~passenger',
            value: 'Roll random passenger.'
          },
          {
            name: '~passengers [world] [parsecs] [steward-DM] [effect]',
            value: 'Roll seeking passengers on the specified world.'
          },
          {
            name: '~roll [formula]',
            value: 'Roll dice as specified in the dice formula *(WIP)*.'
          },
          {
            name: '~sell [price] [effect]',
            value: "Modify a sale price based on a roll's effect."
          },
          {
            name: '~subsector [letter]',
            value: 'Print out the worlds in the specified subsector.'
          },
          {
            name: '~trade [origin] [destination]',
            value: 'Roll trade goods between the two worlds.'
          },
          {
            name: '~world [name]',
            value: 'Print out planetary information about the specified world.'
          }
        ]
      }
    }
  ]
}

const client = new Discord.Client()

client.on('message', msg => {
  if (msg.content.substring(0, 1) === '~') {
    const [cmd, ...args] = msg.content
      .substring(1)
      .split(' ')
      .filter(s => s !== '')

    let out
    switch (cmd) {
      case 'black-market':
        out = Trade.print(args, true)
        break
      case 'buy':
        out = Price.printBuy(parseInt(args[0], 10), parseInt(args[1], 10))
        break
      case 'faction':
        out = Faction.printRandom()
        break
      case 'freight':
        out = Freight.print(args[0], args[1])
        break
      case 'gamble':
        out = Price.printSell(parseInt(args[0], 10), parseInt(args[1], 10))
        break
      case 'government':
        out = Government.print(args[0])
        break
      case 'help':
        out = printHelp()
        break
      case 'name':
        Name.printRandom()
          .then(o => output(msg.channel, o))
          .catch(e => output(msg.channel, e))
        break
      case 'passenger':
        out = Passenger.printRandom()
        break
      case 'passengers':
        out = Passengers.printRandom(...args)
        break
      case 'roll':
        out = Roll.print(args)
        break
      case 'sell':
        out = Price.printSell(parseInt(args[0], 10), parseInt(args[1], 10))
        break
      case 'subsector':
        out = Subsector.print(args[0])
        break
      case 'trade':
        out = Trade.print(args, false)
        break
      case 'world':
        out = World.printInfo(args[0])
        break
    }

    if (out) output(msg.channel, out)
  }
})

client.on('ready', () => {
  console.log(`Connected as ${client.user.username} (${client.user.id})`)
  client.user.setActivity('~help', { type: 'LISTENING' })
})

require('http')
  .createServer()
  .listen(3000)

client.login(process.env.TOKEN)
