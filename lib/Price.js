const Color = require('./Color')
const { d } = require('./utils')

function printBuy (price, effect) {
  const roll = d(3) + effect
  let mod
  if (roll < 0) mod = 200
  else if (roll < 1) mod = 175
  else if (roll < 2) mod = 150
  else if (roll < 3) mod = 135
  else mod = Math.max(25, 140 - roll * 5)
  const roundedPrice = Math.round(mod * 0.01 * price)
  return [
    {
      embed: {
        color: Color.Green,
        title: `(${roll}) ${mod}% -> Cr${roundedPrice}`
      }
    }
  ]
}

function printSell (price, effect) {
  const roll = d(3) + effect
  let mod
  if (roll < 0) mod = 30
  else if (roll < 11) mod = 40 + roll * 5
  else mod = Math.min(160, 100 + roll * 5)
  const roundedPrice = Math.round(mod * 0.01 * price)
  return [
    {
      embed: {
        color: Color.Green,
        title: `(${roll}) ${mod}% -> Cr${roundedPrice}`
      }
    }
  ]
}

module.exports = { printBuy, printSell }
