const Color = require('./Color')
const { ensp, printError } = require('./view')
const { rand } = require('./utils')

function print (args) {
  const rollString = args
    .join('')
    .replace(/ /g, '')
    .replace(/\+-/g, '-')
    .toUpperCase()

  if (rollString.length === 0) {
    return printError('You didn\'t give me anything to roll!')
  }

  const rolls = []
  const result = rollString
    .match(/([*+]?[-]?\d+D?(\d*)?)/g)
    .map(m => {
      let operation
      if (m.startsWith('*-')) operation = (a, b) => a * -b
      else if (m.startsWith('*')) operation = (a, b) => a * b
      else operation = (a, b) => a + b

      const num = Math.abs(parseInt(m.replace(/[+*]/g, ''), 10))

      let die
      if (m.endsWith('D3')) die = () => rand(1, 3)
      else if (m.endsWith('D')) die = () => rand(1, 6)

      let total = 0
      if (die) {
        for (let i = 0; i < num; i++) {
          const roll = die()
          rolls.push(roll)
          total += roll
        }
      } else total = num

      return [operation, total]
    })
    .reduce((x, [op, y]) => op(x, y), 0)

  return [
    {
      embed: {
        color: Color.Green,
        title: `Rolling \`${rollString}\``,
        description: `*(${rolls.join(', ')})*${ensp}➤${ensp}**${result}**`
      }
    }
  ]
}

module.exports = {
  print
}
