const TradeCode = require('./TradeCode')
const TravelCode = require('./TravelCode')
const { d, isin } = require('./utils')

const {
  Ag,
  As,
  De,
  Fl,
  Ga,
  Hi,
  Ht,
  Ie,
  In,
  Lo,
  Lt,
  Na,
  Ni,
  Po,
  Ri,
  Wa
} = TradeCode

const { Amber, Red } = TravelCode

const hasTC = (...codes) => world =>
  world.tradeCodes.some(c => isin(c, ...codes))

const tradeGoods = [
  {
    type: 'Common Electronics',
    availability: () => true,
    tons: r => d(1, r) * 10,
    basePrice: 20000,
    purchaseDMs: [
      { tradeCode: In, dm: 2 },
      { tradeCode: Ht, dm: 3 },
      { tradeCode: Ri, dm: 1 }
    ],
    saleDMs: [
      { tradeCode: Ni, dm: 2 },
      { tradeCode: Lt, dm: 1 },
      { tradeCode: Po, dm: 1 }
    ]
  },
  {
    type: 'Common Industrial Goods',
    availability: () => true,
    tons: r => d(2, r) * 10,
    basePrice: 10000,
    purchaseDMs: [{ tradeCode: Na, dm: 2 }, { tradeCode: In, dm: 5 }],
    saleDMs: [{ tradeCode: Ni, dm: 3 }, { tradeCode: Ag, dm: 2 }]
  },
  {
    type: 'Common Manufactured Goods',
    availability: () => true,
    tons: r => d(2, r) * 10,
    basePrice: 20000,
    purchaseDMs: [{ tradeCode: Na, dm: 2 }, { tradeCode: In, dm: 5 }],
    saleDMs: [{ tradeCode: Ni, dm: 3 }, { tradeCode: Hi, dm: 2 }]
  },
  {
    type: 'Common Raw Materials',
    availability: () => true,
    tons: r => d(2, r) * 20,
    basePrice: 5000,
    purchaseDMs: [{ tradeCode: Ag, dm: 3 }, { tradeCode: Ga, dm: 2 }],
    saleDMs: [{ tradeCode: In, dm: 2 }, { tradeCode: Po, dm: 2 }]
  },
  {
    type: 'Common Consumables',
    availability: () => true,
    tons: r => d(2, r) * 20,
    basePrice: 500,
    purchaseDMs: [
      { tradeCode: Ag, dm: 3 },
      { tradeCode: Wa, dm: 2 },
      { tradeCode: Ga, dm: 1 },
      { tradeCode: As, dm: -4 }
    ],
    saleDMs: [
      { tradeCode: As, dm: 1 },
      { tradeCode: Fl, dm: 1 },
      { tradeCode: Ie, dm: 1 },
      { tradeCode: Hi, dm: 1 }
    ]
  },
  {
    type: 'Common Ore',
    availability: () => true,
    tons: r => d(2, r) * 20,
    basePrice: 1000,
    purchaseDMs: [{ tradeCode: As, dm: 4 }],
    saleDMs: [{ tradeCode: In, dm: 3 }, { tradeCode: Ni, dm: 1 }]
  },
  {
    type: 'Advanced Electronics',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 100000,
    purchaseDMs: [{ tradeCode: In, dm: 2 }, { tradeCode: Ht, dm: 3 }],
    saleDMs: [
      { tradeCode: Ni, dm: 1 },
      { tradeCode: Ri, dm: 2 },
      { tradeCode: As, dm: 3 }
    ]
  },
  {
    type: 'Advanced Machine Parts',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 75000,
    purchaseDMs: [{ tradeCode: In, dm: 2 }, { tradeCode: Ht, dm: 1 }],
    saleDMs: [{ tradeCode: As, dm: 2 }, { tradeCode: Ni, dm: 1 }]
  },
  {
    type: 'Advanced Manufactured Goods',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 100000,
    purchaseDMs: [{ tradeCode: In, dm: 1 }],
    saleDMs: [{ tradeCode: Hi, dm: 1 }, { tradeCode: Ri, dm: 2 }]
  },
  {
    type: 'Advanced Weapons',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 150000,
    purchaseDMs: [{ tradeCode: Ht, dm: 2 }],
    saleDMs: [
      { tradeCode: Po, dm: 1 },
      { travelCode: Amber, dm: 2 },
      { travelCode: Red, dm: 4 }
    ]
  },
  {
    type: 'Advanced Vehicles',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 180000,
    purchaseDMs: [{ tradeCode: Ht, dm: 2 }],
    saleDMs: [{ tradeCode: As, dm: 2 }, { tradeCode: Ri, dm: 2 }]
  },
  {
    type: 'Biochemicals',
    availability: hasTC(Ag, Wa),
    tons: r => d(1, r) * 5,
    basePrice: 50000,
    purchaseDMs: [{ tradeCode: Ag, dm: 1 }, { tradeCode: Wa, dm: 2 }],
    saleDMs: [{ tradeCode: In, dm: 2 }]
  },
  {
    type: 'Crystals & Gems',
    availability: hasTC(As, De, Ie),
    tons: r => d(1, r) * 5,
    basePrice: 20000,
    purchaseDMs: [
      { tradeCode: As, dm: 2 },
      { tradeCode: De, dm: 1 },
      { tradeCode: Ie, dm: 1 }
    ],
    saleDMs: [{ tradeCode: In, dm: 3 }, { tradeCode: Ri, dm: 2 }]
  },
  {
    type: 'Cybernetics',
    availability: hasTC(Ht),
    tons: r => d(1, r),
    basePrice: 25000,
    purchaseDMs: [{ tradeCode: Ht, dm: 1 }],
    saleDMs: [
      { tradeCode: As, dm: 1 },
      { tradeCode: Ie, dm: 1 },
      { tradeCode: Ri, dm: 2 }
    ]
  },
  {
    type: 'Live Animals',
    availability: hasTC(Ag, Ga),
    tons: r => d(1, r) * 10,
    basePrice: 10000,
    purchaseDMs: [{ tradeCode: Ag, dm: 2 }],
    saleDMs: [{ tradeCode: Lo, dm: 3 }]
  },
  {
    type: 'Luxury Consumables',
    availability: hasTC(Ag, Ga, Wa),
    tons: r => d(1, r) * 10,
    basePrice: 20000,
    purchaseDMs: [{ tradeCode: Ag, dm: 2 }, { tradeCode: Wa, dm: 1 }],
    saleDMs: [{ tradeCode: Ri, dm: 2 }, { tradeCode: Hi, dm: 2 }]
  },
  {
    type: 'Luxury Goods',
    availability: hasTC(Hi),
    tons: r => d(1, r),
    basePrice: 20000,
    purchaseDMs: [{ tradeCode: Hi, dm: 1 }],
    saleDMs: [{ tradeCode: Ri, dm: 4 }]
  },
  {
    type: 'Medical Supplies',
    availability: hasTC(Ht + 2, Hi),
    tons: r => d(1, r) * 5,
    basePrice: 50000,
    purchaseDMs: [{ tradeCode: Ht, dm: 2 }],
    saleDMs: [
      { tradeCode: In, dm: 2 },
      { tradeCode: Po, dm: 1 },
      { tradeCode: Ri, dm: 1 }
    ]
  },
  {
    type: 'Petrochemicals',
    availability: hasTC(De, Fl, Ie, Wa),
    tons: r => d(1, r) * 10,
    basePrice: 10000,
    purchaseDMs: [{ tradeCode: De, dm: 2 }],
    saleDMs: [
      { tradeCode: In, dm: 2 },
      { tradeCode: Ag, dm: 1 },
      { tradeCode: Lt, dm: 2 }
    ]
  },
  {
    type: 'Pharmaceuticals',
    availability: hasTC(As, De, Hi, Wa),
    tons: r => d(1, r),
    basePrice: 10000,
    purchaseDMs: [{ tradeCode: As, dm: 2 }, { tradeCode: Hi, dm: 1 }],
    saleDMs: [{ tradeCode: Ri, dm: 2 }, { tradeCode: Lt, dm: 1 }]
  },
  {
    type: 'Polymers',
    availability: hasTC(In),
    tons: r => d(1, r) * 10,
    basePrice: 7000,
    purchaseDMs: [{ tradeCode: In, dm: 1 }],
    saleDMs: [{ tradeCode: Ri, dm: 2 }, { tradeCode: Ni, dm: 1 }]
  },
  {
    type: 'Precious Metals',
    availability: hasTC(As, De, Ie, Fl),
    tons: r => d(1, r),
    basePrice: 5000,
    purchaseDMs: [
      { tradeCode: As, dm: 3 },
      { tradeCode: De, dm: 1 },
      { tradeCode: Ie, dm: 2 }
    ],
    saleDMs: [
      { tradeCode: Ri, dm: 3 },
      { tradeCode: In, dm: 2 },
      { tradeCode: Ht, dm: 1 }
    ]
  },
  {
    type: 'Radioactives',
    availability: hasTC(As, De, Lo),
    tons: r => d(1, r),
    basePrice: 100000,
    purchaseDMs: [{ tradeCode: As, dm: 2 }, { tradeCode: Lo, dm: 2 }],
    saleDMs: [
      { tradeCode: In, dm: 3 },
      { tradeCode: Ht, dm: 1 },
      { tradeCode: Ni, dm: -2 },
      { tradeCode: Ag, dm: -3 }
    ]
  },
  {
    type: 'Robots',
    availability: hasTC(In),
    tons: r => d(1, r) * 5,
    basePrice: 400000,
    purchaseDMs: [{ tradeCode: In, dm: 1 }],
    saleDMs: [{ tradeCode: Ag, dm: 2 }, { tradeCode: Ht, dm: 1 }]
  },
  {
    type: 'Spices',
    availability: hasTC(Ga, De, Wa),
    tons: r => d(1, r) * 10,
    basePrice: 6000,
    purchaseDMs: [{ tradeCode: De, dm: 2 }],
    saleDMs: [
      { tradeCode: Hi, dm: 2 },
      { tradeCode: Ri, dm: 3 },
      { tradeCode: Po, dm: 3 }
    ]
  },
  {
    type: 'Textiles',
    availability: hasTC(Ag, Ni),
    tons: r => d(1, r) * 20,
    basePrice: 3000,
    purchaseDMs: [{ tradeCode: Ag, dm: 7 }],
    saleDMs: [{ tradeCode: Hi, dm: 3 }, { tradeCode: Na, dm: 2 }]
  },
  {
    type: 'Uncommon Ore',
    availability: hasTC(As, Ie),
    tons: r => d(1, r) * 20,
    basePrice: 5000,
    purchaseDMs: [{ tradeCode: As, dm: 4 }],
    saleDMs: [{ tradeCode: In, dm: 3 }, { tradeCode: Ni, dm: 1 }]
  },
  {
    type: 'Uncommon Raw Materials',
    availability: hasTC(Ag, De, Wa),
    tons: r => d(1, r) * 10,
    basePrice: 20000,
    purchaseDMs: [{ tradeCode: Ag, dm: 2 }, { tradeCode: Wa, dm: 1 }],
    saleDMs: [{ tradeCode: In, dm: 2 }, { tradeCode: Ht, dm: 1 }]
  },
  {
    type: 'Wood',
    availability: hasTC(Ag, Ga),
    tons: r => d(1, r) * 20,
    basePrice: 1000,
    purchaseDMs: [{ tradeCode: Ag, dm: 6 }],
    saleDMs: [{ tradeCode: Ri, dm: 2 }, { tradeCode: In, dm: 1 }]
  },
  {
    type: 'Vehicles',
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 10,
    basePrice: 15000,
    purchaseDMs: [{ tradeCode: In, dm: 2 }, { tradeCode: Ht, dm: 1 }],
    saleDMs: [{ tradeCode: Ni, dm: 2 }, { tradeCode: Hi, dm: 1 }]
  },
  {
    type: 'Illegal Biochemicals',
    illegal: true,
    availability: hasTC(Ag, Wa),
    tons: r => d(1, r) * 5,
    basePrice: 50000,
    purchaseDMs: [{ tradeCode: Wa, dm: 2 }],
    saleDMs: [{ tradeCode: In, dm: 6 }]
  },
  {
    type: 'Illegal Cybernetics',
    illegal: true,
    availability: hasTC(Ht),
    tons: r => d(1, r),
    basePrice: 25000,
    purchaseDMs: [{ tradeCode: Ht, dm: 1 }],
    saleDMs: [
      { tradeCode: As, dm: 4 },
      { tradeCode: Ie, dm: 4 },
      { tradeCode: Ri, dm: 8 },
      { travelCode: Amber, dm: 6 },
      { travelCode: Red, dm: 6 }
    ]
  },
  {
    type: 'Illegal Drugs',
    illegal: true,
    availability: hasTC(As, De, Hi, Wa),
    tons: r => d(1, r),
    basePrice: 10000,
    purchaseDMs: [
      { tradeCode: As, dm: 1 },
      { tradeCode: De, dm: 1 },
      { tradeCode: Ga, dm: 1 },
      { tradeCode: Wa, dm: 1 }
    ],
    saleDMs: [{ tradeCode: Ri, dm: 6 }, { tradeCode: Hi, dm: 6 }]
  },
  {
    type: 'Illegal Luxuries',
    illegal: true,
    availability: hasTC(Ag, Ga, Wa),
    tons: r => d(1, r),
    basePrice: 5000,
    purchaseDMs: [{ tradeCode: Ag, dm: 2 }, { tradeCode: Wa, dm: 1 }],
    saleDMs: [{ tradeCode: Ri, dm: 6 }, { tradeCode: Hi, dm: 4 }]
  },
  {
    type: 'Illegal Weapons',
    illegal: true,
    availability: hasTC(In, Ht),
    tons: r => d(1, r) * 5,
    basePrice: 150000,
    purchaseDMs: [{ tradeCode: Ht, dm: 2 }],
    saleDMs: [
      { tradeCode: Po, dm: 6 },
      { travelCode: Amber, dm: 8 },
      { travelCode: Red, dm: 10 }
    ]
  }
]

module.exports = tradeGoods
