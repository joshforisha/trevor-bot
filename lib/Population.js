const { rand } = require('./utils')

function description (population) {
  switch (population) {
    case 0:
      return 'None'
    case 1:
      return 'Dozens'
    case 2:
      return 'Hundreds'
    case 3:
      return 'Thousands'
    case 4:
      return 'Tens of thousands'
    case 5:
      return 'Hundreds of thousands'
    case 6:
      return 'Millions'
    case 7:
      return 'Tens of millions'
    case 8:
      return 'Hundreds of millions'
    case 9:
      return 'Billions'
    case 10:
      return 'Tens of billions'
    case 11:
      return 'Hundreds of billions'
    case 12:
      return 'Trillions'
  }
}

function generateNumber (population, rnd) {
  switch (population) {
    case 0:
      return 0
    case 1:
      return rand(1, 99, rnd).toLocaleString()
    case 2:
      return (rand(10, 99, rnd) * 10).toLocaleString()
    case 3:
      return (rand(10, 99, rnd) * 100).toLocaleString()
    case 4:
      return (rand(10, 99, rnd) * 1e3).toLocaleString()
    case 5:
      return (rand(10, 99, rnd) * 1e4).toLocaleString()
    case 6:
      return `${rand(1, 10, rnd)}.${rand(1, 10, rnd)} million`
    case 7:
      return `${rand(10, 99, rnd)} million`
    case 8:
      return `${rand(10, 99, rnd) * 10} million`
    case 9:
      return `${rand(1, 10, rnd)}.${rand(1, 10, rnd)} billion`
    case 10:
      return `${rand(10, 99, rnd)} billion`
    case 11:
      return `${rand(10, 99, rnd) * 10} billion`
    case 12:
      return `${rand(1, 10, rnd)}.${rand(1, 10, rnd)} trillion`
  }
}

module.exports = {
  description,
  generateNumber
}
