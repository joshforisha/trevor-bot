const { d } = require('./utils')

function facilities (port) {
  switch (port) {
    case 'A':
      return 'Shipyard (all), Repair'
    case 'B':
      return 'Shipyard (spacecraft), Repair'
    case 'F':
    case 'C':
      return 'Shipyard (small craft), Repair'
    case 'G':
    case 'D':
      return 'Limited Repair'
    case 'H':
    case 'E':
    case 'X':
    case 'Y':
      return '*None*'
  }
}

function fuel (port) {
  if (port === 'A' || port === 'B' || port === 'F') {
    return 'Refined Fuel (Cr500)'
  }
  return 'Unrefined Fuel (Cr100)'
}

function quality (port) {
  switch (port) {
    case 'A':
      return 'Excellent'
    case 'B':
      return 'Good'
    case 'F':
      return 'Fair'
    case 'C':
      return 'Routine'
    case 'G':
      return 'Mediocre'
    case 'D':
      return 'Poor'
    case 'H':
      return 'Improvised'
    case 'E':
      return 'Frontier'
    case 'X':
    case 'Y':
      return 'No Port'
  }
}

function generateBerthingCost (port, rnd) {
  switch (port) {
    case 'A':
      return `Cr${(d(1, rnd) * 1000).toLocaleString()}`
    case 'B':
      return `Cr${(d(1, rnd) * 500).toLocaleString()}`
    case 'F':
      return `Cr${(d(1, rnd) * 300).toLocaleString()}`
    case 'C':
      return `Cr${(d(1, rnd) * 100).toLocaleString()}`
    case 'G':
      return `Cr${(d(1, rnd) * 55).toLocaleString()}`
    case 'D':
      return `Cr${(d(1, rnd) * 10).toLocaleString()}`
    case 'H':
      return `Cr${(d(1, rnd) * 5).toLocaleString()}`
    case 'E':
    case 'X':
      return 'Cr0'
  }
}

module.exports = {
  facilities,
  fuel,
  generateBerthingCost,
  quality
}
