const Color = require('./Color')
const { draw } = require('./utils')

function printRandom () {
  const passenger = draw(passengers)
  return [
    {
      embed: {
        color: Color.Green,
        title: passenger
      }
    }
  ]
}

const passengers = [
  'Refugee – political',
  'Refugee - economic',
  'Starting a new life offworld',
  'Mercenary',
  'Spy',
  'Corporate executive',
  'Out to see the universe',
  'Tourist (1–3: Irritating, 4–6: Charming)',
  'Wide-eyed yokel',
  'Adventurer',
  'Explorer',
  'Claustrophobic',
  'Expectant Mother',
  'Wants to stowaway or join the crew',
  'Possesses something dangerous or illegal',
  'Causes trouble (1–3: Drunkard, 4–5: Violent, 6: Insane)',
  'Unusually pretty or handsome',
  'Engineer (Mechanic and Engineer of 1D-1 each)',
  'Ex-scout',
  'Wanderer',
  'Thief or other criminal',
  'Scientist',
  'Journalist or researcher',
  'Entertainer (Steward and Perform of 1D-1 each)',
  'Gambler (Gambling skill of 1D-1)',
  'Rich noble – complains a lot',
  'Rich noble - eccentric',
  'Rich noble – raconteur',
  'Diplomat on a mission',
  'Agent on a mission',
  'Patron',
  'Alien',
  'Bounty Hunter',
  'On the run',
  'Wants to be on board the Travellers’ ship for some reason',
  'Hijacker or pirate agent'
]

module.exports = {
  printRandom
}
