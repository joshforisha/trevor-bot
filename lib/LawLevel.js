function bannedItems (lawLevel) {
  switch (lawLevel) {
    case 0:
      return 'No restrictions'
    case 1:
      return 'Poison gas, explosives, undetectable weapons, WMD; Battle dress'
    case 2:
      return 'Portable energy and laser weapons; Combat armour'
    case 3:
      return 'Military weapons; Flak armour'
    case 4:
      return 'Light assault weapons and submachine guns; Cloth armour'
    case 5:
      return 'Personal concealable weapons; Mesh armour'
    case 6:
      return 'All firearms except shotguns & stunners'
    case 7:
      return 'Shotguns'
    case 8:
      return 'All bladed weapons, stunners; All visible armour'
    default:
      return 'All weapons; All armour'
  }
}

module.exports = {
  bannedItems
}
