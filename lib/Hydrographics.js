const { rand } = require('./utils')

function description (hydrographics) {
  switch (hydrographics) {
    case 0:
      return 'Desert world'
    case 1:
      return 'Dry world'
    case 2:
      return 'A few small seas'
    case 3:
      return 'Small seas and oceans'
    case 4:
      return 'Wet world'
    case 5:
      return 'Large oceans'
    case 6:
      return 'Prominent oceans'
    case 7:
      return 'Earth-like world'
    case 8:
      return 'Water world'
    case 9:
      return 'Only a few small islands and archipelagos'
    case 10:
      return 'Almost entirely water'
  }
}

function generatePercentage (hydrographics, rnd) {
  switch (hydrographics) {
    case 0:
      return `${rand(0, 5, rnd)}%`
    case 1:
      return `${rand(6, 15, rnd)}%`
    case 2:
      return `${rand(16, 25, rnd)}%`
    case 3:
      return `${rand(26, 35, rnd)}%`
    case 4:
      return `${rand(36, 45, rnd)}%`
    case 5:
      return `${rand(46, 55, rnd)}%`
    case 6:
      return `${rand(56, 65, rnd)}%`
    case 7:
      return `${rand(66, 75, rnd)}%`
    case 8:
      return `${rand(76, 85, rnd)}%`
    case 9:
      return `${rand(86, 95, rnd)}%`
    case 10:
      return `${rand(96, 100, rnd)}%`
  }
}

module.exports = {
  description,
  generatePercentage
}
