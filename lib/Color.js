const Color = {
  Blue: 0x5487ba,
  Green: 0xb9cb65,
  Red: 0xca424f
}

module.exports = Color
