const Color = require('./Color')
const World = require('./World')
const bahamutSector = require('./bahamut-sector.json')
const { countString, d } = require('./utils')
const { printError } = require('./view')

function priceBasic (parsecs) {
  switch (parsecs) {
    case 1:
      return 2200
    case 2:
      return 2900
    case 3:
      return 4400
    case 4:
      return 8600
    case 5:
      return 9400
    case 6:
      return 93000
  }
}

function priceHigh (parsecs) {
  switch (parsecs) {
    case 1:
      return 8500
    case 2:
      return 12000
    case 3:
      return 20000
    case 4:
      return 41000
    case 5:
      return 45000
    case 6:
      return 470000
  }
}

function priceLow (parsecs) {
  switch (parsecs) {
    case 1:
      return 700
    case 2:
      return 1300
    case 3:
      return 2200
    case 4:
      return 4300
    case 5:
      return 13000
    case 6:
      return 96000
  }
}

function priceMiddle (parsecs) {
  switch (parsecs) {
    case 1:
      return 6200
    case 2:
      return 9000
    case 3:
      return 15000
    case 4:
      return 31000
    case 5:
      return 34000
    case 6:
      return 350000
  }
}

function printRandom (worldName, inParsecs, stewardDM, skillEffect) {
  const lowerWorldName = worldName.toLowerCase()
  const world = bahamutSector.find(
    w => w.name.toLowerCase() === lowerWorldName
  )
  const parsecs = parseInt(inParsecs, 10)

  if (typeof world === 'undefined') {
    return printError(
      `Sorry, I can't find a world in the Bahamut Sector named "${worldName}".`
    )
  }

  const { population, starport } = World.properties(world)

  let baseDM = parseInt(stewardDM, 10) + skillEffect

  if (population < 2) baseDM -= 4
  else if (population === 6 || population === 7) baseDM += 1
  else if (population >= 8) baseDM += 3

  if (starport === 'A') baseDM += 2
  else if (starport === 'B') baseDM += 1
  else if (starport === 'E') baseDM -= 1
  else if (starport === 'X') baseDM -= 3

  if (world.travelCode === 'Amber') baseDM += 1
  else if (world.travelCode === 'Red') baseDM -= 4

  const lowPassengers = traffic(baseDM + 1)
  const lowPassage = countString(lowPassengers, 'passenger', 'passengers')
  const lowCost = priceLow(parsecs)

  const basicPassengers = traffic(baseDM)
  const basicPassage = countString(basicPassengers, 'passenger', 'passengers')
  const basicCost = priceBasic(parsecs)

  const middlePassengers = traffic(baseDM)
  const middlePassage = countString(
    middlePassengers,
    'passenger',
    'passengers'
  )
  const middleCost = priceMiddle(parsecs)

  const highPassengers = traffic(baseDM - 4)
  const highPassage = countString(highPassengers, 'passenger', 'passengers')
  const highCost = priceHigh(parsecs)

  return [
    {
      embed: {
        color: Color.Green,
        title: `Seeking passengers on ${world.name} for ${parsecs} parsecs`,
        fields: [
          {
            name: `Low Passage (Cr${lowCost} each)`,
            value: lowPassage
          },
          {
            name: `Basic Passage (Cr${basicCost} each)`,
            value: basicPassage
          },
          {
            name: `Middle Passage (Cr${middleCost} each)`,
            value: middlePassage
          },
          {
            name: `High Passage (Cr${highCost} each)`,
            value: highPassage
          }
        ]
      }
    }
  ]
}

function traffic (roll) {
  if (roll < 2) return 0
  if (roll < 4) return d(1)
  if (roll < 7) return d(2)
  if (roll < 11) return d(3)
  if (roll < 14) return d(4)
  if (roll < 16) return d(5)
  if (roll < 17) return d(6)
  if (roll < 18) return d(7)
  if (roll < 19) return d(8)
  if (roll < 20) return d(9)
  return d(10)
}

module.exports = { printRandom }
