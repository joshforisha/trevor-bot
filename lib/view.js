const Color = require('./Color')
const { initialize, padLeft, padRight } = require('./utils')

const emsp = ' '

const ensp = ' '

const letters = [
  '',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P'
]

function printError (message) {
  return [
    {
      embed: {
        color: Color.Red,
        title: message
      }
    }
  ]
}

function tabularize (headers, data, rightAligns = []) {
  const allData = [headers, ...data]

  const widths = allData.reduce(
    (ws, cs) => ws.map((w, i) => Math.max(w, cs[i].length)),
    initialize(headers.length, 0)
  )

  const lines = allData.map(xs =>
    xs
      .map((x, i) =>
        rightAligns.indexOf(i) > -1
          ? padLeft(x, widths[i], ' ')
          : padRight(x, widths[i], ' ')
      )
      .join('   ')
  )

  let t = 0
  const tables = ['']
  for (let i = 0; i < lines.length; i++) {
    if (tables[t].length + lines[i].length >= 2000) {
      tables[++t] = lines[i]
    } else {
      tables[t] += '\n' + lines[i]
    }
  }

  return tables
}

module.exports = {
  emsp,
  ensp,
  letters,
  printError,
  tabularize
}
