const Color = require('./Color')
const Https = require('https')
const { printError } = require('./view')

function generate () {
  return new Promise((resolve, reject) => {
    const req = Https.request(
      {
        hostname: 'www.behindthename.com',
        method: 'GET',
        path: `/api/random.json?key=${process.env.BTN_KEY}&number=1&randomsurname=yes`
      },
      res => {
        const data = []
        res.setEncoding('utf8')

        res.on('data', chunk => {
          data.push(chunk)
        })

        res.on('error', error => {
          reject(error)
        })

        res.on('end', () => {
          const gen = JSON.parse(data.join(''))

          if ('names' in gen) {
            resolve(gen.names.join(' '))
          } else {
            reject(new Error(`Weird name ${JSON.stringify(gen)}`))
          }
        })
      }
    )
    req.end()
  })
}

function printRandom () {
  return generate()
    .then(name => [
      {
        embed: {
          color: Color.green,
          title: name
        }
      }
    ])
    .catch(error => {
      console.error(error)
      printError('Sorry, I couldn\'t come up with a random name.')
    })
}

module.exports = {
  generate,
  printRandom
}
