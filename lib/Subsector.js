const Color = require('./Color')
const bahamutSector = require('./bahamut-sector.json')
const { letters, printError, tabularize } = require('./view')
const { uwpArray } = require('./World')

function print (subsector) {
  if (typeof subsector !== 'string') {
    return printError('You didn\'t give me a *subsector letter* to lookup!')
  }

  const sub = subsector.toUpperCase()

  const subsectorWorlds = bahamutSector.filter(
    w => letters[w.subsector] === sub
  )

  if (subsectorWorlds.length === 0) {
    return printError(`Sorry, I can't find any worlds in subsector "${sub}".`)
  }

  const worlds = subsectorWorlds
    .sort((a, b) => (a.name < b.name ? -1 : 1))
    .map(w => uwpArray(w))

  const worldTables = tabularize(
    ['World', 'Hex', 'UWP', 'Bases', 'Trade', 'Travel', 'Allegiance'],
    worlds
  )

  return [
    {
      embed: {
        color: Color.Green,
        title: `Subsector ${sub}`,
        description: `${worlds.length} worlds`
      }
    },
    ...worldTables.map(tbl => ({ content: `\`\`\`${tbl}\`\`\`` }))
  ]
}

module.exports = { print }
