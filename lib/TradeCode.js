const { between, isin } = require('./utils')

const TradeCode = {
  Ag: 'Ag',
  As: 'As',
  Ba: 'Ba',
  De: 'De',
  Fl: 'Fl',
  Ga: 'Ga',
  Hi: 'Hi',
  Ht: 'Ht',
  Ie: 'Ie',
  In: 'In',
  Lo: 'Lo',
  Lt: 'Lt',
  Na: 'Na',
  Ni: 'Ni',
  Po: 'Po',
  Ri: 'Ri',
  Va: 'Va',
  Wa: 'Wa'
}

function classify ({
  size,
  atmosphere,
  hydrographics,
  population,
  government,
  lawLevel,
  techLevel
}) {
  const codes = []

  if (
    between(atmosphere, 4, 9) &&
    between(hydrographics, 4, 8) &&
    between(population, 5, 7)
  ) {
    codes.push(TradeCode.Ag)
  }

  if (size === 0 && atmosphere === 0 && hydrographics === 0) {
    codes.push(TradeCode.As)
  }

  if (population === 0 && government === 0 && lawLevel === 0) {
    codes.push(TradeCode.Ba)
  }

  if (atmosphere >= 2 && hydrographics === 0) {
    codes.push(TradeCode.De)
  }

  if (atmosphere >= 10 && hydrographics >= 1) {
    codes.push(TradeCode.Fl)
  }

  if (
    between(size, 6, 8) &&
    isin(atmosphere, 5, 6, 8) &&
    between(hydrographics, 5, 7)
  ) {
    codes.push(TradeCode.Ga)
  }

  if (population >= 9) {
    codes.push(TradeCode.Hi)
  }

  if (techLevel >= 12) {
    codes.push(TradeCode.Ht)
  }

  if (between(atmosphere, 0, 1) && hydrographics >= 1) {
    codes.push(TradeCode.Ie)
  }

  if (isin(atmosphere, [0, 1, 2, 4, 7, 9]) && population >= 9) {
    codes.push(TradeCode.In)
  }

  if (population <= 3) {
    codes.push(TradeCode.Lo)
  }

  if (techLevel <= 5) {
    codes.push(TradeCode.Lt)
  }

  if (
    between(atmosphere, 0, 3) &&
    between(hydrographics, 0, 3) &&
    population >= 6
  ) {
    codes.push(TradeCode.Na)
  }

  if (between(population, 0, 6)) {
    codes.push(TradeCode.Ni)
  }

  if (between(atmosphere, 2, 5) && between(hydrographics, 0, 3)) {
    codes.push(TradeCode.Po)
  }

  if (
    isin(atmosphere, [6, 8]) &&
    between(population, 6, 8) &&
    between(government, 4, 9)
  ) {
    codes.push(TradeCode.Ri)
  }

  if (atmosphere === 0) {
    codes.push(TradeCode.Va)
  }

  if (hydrographics >= 10) {
    codes.push(TradeCode.Wa)
  }

  return codes
}

function name (tradeCode) {
  switch (tradeCode) {
    case 'Ag':
      return 'Agricultural'
    case 'As':
      return 'Asteroid'
    case 'Ba':
      return 'Barren'
    case 'De':
      return 'Desert'
    case 'Fl':
      return 'Fluid Oceans'
    case 'Ga':
      return 'Garden'
    case 'Hi':
      return 'High Population'
    case 'Ht':
      return 'High Tech'
    case 'Ie':
      return 'Ice-Capped'
    case 'In':
      return 'Industrial'
    case 'Lo':
      return 'Low Population'
    case 'Lt':
      return 'Low Tech'
    case 'Na':
      return 'Non-Agricultural'
    case 'Ni':
      return 'Non-Industrial'
    case 'Po':
      return 'Poor'
    case 'Ri':
      return 'Rich'
    case 'Va':
      return 'Vacuum'
    case 'Wa':
      return 'Water World'
  }
}

module.exports = Object.assign({}, TradeCode, {
  classify,
  name
})
