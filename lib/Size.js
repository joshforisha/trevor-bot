const { rand, randq, unit } = require('./utils')

const Day = 86400
const Hour = 3600
const Minute = 60
const Week = 604800

function generateDiameter (size, rnd) {
  switch (size) {
    case 0:
      return `${(rand(6, 10, rnd) * 100).toLocaleString()} km`
    case 1:
      return `${(rand(11, 16, rnd) * 100).toLocaleString()} km`
    case 2:
      return `${(rand(17, 32, rnd) * 100).toLocaleString()} km`
    case 3:
      return `${(rand(33, 48, rnd) * 100).toLocaleString()} km`
    case 4:
      return `${(rand(49, 64, rnd) * 100).toLocaleString()} km`
    case 5:
      return `${(rand(65, 80, rnd) * 100).toLocaleString()} km`
    case 6:
      return `${(rand(81, 96, rnd) * 100).toLocaleString()} km`
    case 7:
      return `${(rand(97, 112, rnd) * 100).toLocaleString()} km`
    case 8:
      return `${(rand(113, 128, rnd) * 100).toLocaleString()} km`
    case 9:
      return `${(rand(129, 144, rnd) * 100).toLocaleString()} km`
    case 10:
      return `${(rand(145, 160, rnd) * 100).toLocaleString()} km`
  }
}

function generateRotationPeriod (size, rnd) {
  switch (size) {
    case 0:
      return time(randq(2, 30 * Minute, 300 * Minute, rnd))
    case 1:
      return time(randq(2, 1 * Hour, 10 * Hour, rnd))
    case 2:
      return time(randq(2, 2 * Hour, 20 * Hour, rnd))
    case 3:
      return time(randq(2, 4 * Hour, 40 * Hour, rnd))
    case 4:
      return time(randq(2, 8 * Hour, 80 * Hour, rnd))
    case 5:
      return time(randq(2, 12 * Hour, 120 * Hour, rnd))
    case 6:
      return time(randq(2, 14 * Hour, 140 * Hour, rnd))
    case 7:
      return time(randq(2, 16 * Hour, 160 * Hour, rnd))
    case 8:
      return time(randq(2, 12 * Hour, 200 * Hour, rnd))
    case 9:
      return time(randq(2, 30 * Hour, 300 * Hour, rnd))
    case 10:
      return time(randq(2, 50 * Hour, 500 * Hour, rnd))
  }
}

function gravity (size) {
  if (size === 0) return 'No Gravity'
  if (size <= 6) return 'Low Gravity'
  if (size <= 9) return 'Standard Gravity'
  return 'High Gravity'
}

function name (size) {
  if (size === 0) return 'Tiny'
  if (size === 1) return 'Very Small'
  if (size <= 4) return 'Small'
  if (size <= 7) return 'Medium'
  if (size <= 9) return 'Large'
  return 'Huge'
}

function time (sec) {
  const s = unit(sec, 's')

  if (sec > 2 * Week) return `${s} (${(sec / Week).toFixed(1)} weeks)`
  if (sec > 2 * Day) return `${s} (${(sec / Day).toFixed(1)} days)`
  if (sec > 2 * Hour) return `${s} (${(sec / Hour).toFixed(1)} hours)`
  if (sec > 2 * Minute) return `${s} (${(sec / Minute).toFixed(1)} minutes)`

  return s
}

module.exports = {
  generateDiameter,
  generateRotationPeriod,
  gravity,
  name
}
