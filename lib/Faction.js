const Color = require('./Color')
const { draw } = require('./utils')

const adjectives = [
  'Sinister',
  'Controlling',
  'Secretive',
  'Popular',
  'Rapacious',
  'Austere',
  'Wealthy',
  'Honorable',
  'Violent',
  'Degenerate',
  'Defiant',
  'Tyrannical',
  'Brutal',
  'Underhanded',
  'Ancient',
  'Terrifying',
  'Fanatic',
  'Brave'
]

const focuses = [
  'expansionist',
  'scientific',
  'authoritarian',
  'colonizing',
  'trade',
  'mercenary',
  'merchant',
  'industrious',
  'criminal',
  'military',
  'anarchist',
  'starfaring',
  'rebel',
  'high tech',
  'diplomatic',
  'corporate',
  'regimented',
  'political'
]

const nouns = [
  'government',
  'cult',
  'fleet',
  'council',
  'corporation',
  'federation',
  'society',
  'coalition',
  'armada',
  'network',
  'consortium',
  'republic',
  'empire',
  'religion',
  'regime',
  'cartel',
  'alliance',
  'legion'
]

function printRandom () {
  const faction = [
    draw(adjectives),
    draw(focuses),
    draw(nouns)
  ].join(' ')

  return [
    {
      embed: {
        color: Color.Green,
        title: faction
      }
    }
  ]
}

module.exports = {
  printRandom
}
