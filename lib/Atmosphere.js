const { d, isin, rand } = require('./utils')

function name (atmosphere) {
  switch (atmosphere) {
    case 0:
      return 'None'
    case 1:
      return 'Trace'
    case 2:
      return 'Very Thin, Tainted'
    case 3:
      return 'Very Thin'
    case 4:
      return 'Thin, Tainted'
    case 5:
      return 'Thin'
    case 6:
      return 'Standard'
    case 7:
      return 'Standard, Tainted'
    case 8:
      return 'Dense'
    case 9:
      return 'Dense, Tainted'
    case 10:
      return 'Exotic'
    case 11:
      return 'Corrosive'
    case 12:
      return 'Insidious'
    case 13:
      return 'Very dense'
    case 14:
      return 'Low'
    case 15:
      return 'Unusual'
  }
}

function generateTemperature (atmosphere, rnd) {
  let dm = 0
  if (isin(atmosphere, 2, 3)) dm = -2
  else if (isin(atmosphere, 4, 5, 14)) dm = -1
  else if (isin(atmosphere, 8, 9)) dm = 1
  else if (isin(atmosphere, 10, 13, 15)) dm = 2
  else if (isin(atmosphere, 11, 12)) dm = 6

  let average = 0
  const res = d(2, rnd) + dm
  if (res <= 2) average = rand(-100, -51, rnd)
  else if (res <= 4) average = rand(-50, 0, rnd)
  else if (res <= 9) average = rand(0, 30, rnd)
  else if (res <= 11) average = rand(31, 80, rnd)
  else average = rand(81, 100, rnd)

  let spread = 10
  if (atmosphere <= 1) spread = rand(41, 80, rnd)
  else if (atmosphere <= 3 || atmosphere === 14) spread = rand(9, 13, rnd)
  else if (atmosphere <= 5) spread = rand(7, 11, rnd)
  else if (atmosphere <= 7) spread = rand(5, 9, rnd)
  else if (atmosphere <= 9) spread = rand(3, 7, rnd)
  else if (atmosphere === 13) spread = rand(1, 5, rnd)
  else spread = rand(1, 9, rnd)

  return `${average - spread}–${average + spread} °C`
}

function requirement (atmosphere) {
  switch (atmosphere) {
    case 0:
    case 1:
    case 12:
      return 'Requires Vacc Suit'
    case 2:
      return 'Requires Respirator, Filter'
    case 3:
      return 'Requires Respirator'
    case 4:
    case 7:
    case 9:
      return 'Requires Filter'
    case 10:
    case 11:
      return 'Requires Air Supply'
  }
}

module.exports = {
  generateTemperature,
  name,
  requirement
}
