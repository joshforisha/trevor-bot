const Contraband = {
  Computers: 'Computers',
  Drugs: 'Drugs',
  None: 'None',
  Psionics: 'Psionics',
  Technology: 'Technology',
  Travellers: 'Travellers',
  Weapons: 'Weapons'
}

module.exports = Contraband
