const Color = require('./Color')
const World = require('./World')
const bahamutSector = require('./bahamut-sector.json')
const { between, con, countString, d, initialize } = require('./utils')
const { printError } = require('./view')

function collectLots (num, fn) {
  const lotWeights = initialize(num, fn)
    .map(tons => tons.toString(10))
    .reduce(
      (acc, tons) =>
        Object.assign({}, acc, {
          [tons]: tons in acc ? acc[tons] + 1 : 1
        }),
      {}
    )

  return Object.keys(lotWeights)
    .sort((a, b) => (parseInt(a, 10) < parseInt(b, 10) ? 1 : -1))
    .map(
      tons =>
        `${countString(parseInt(tons, 10), 'ton')} (${countString(
          lotWeights[tons],
          'lot'
        )})`
    )
    .join('\n')
}

function numLots (traffic) {
  switch (con(traffic, 1, 20)) {
    case 1:
      return 0
    case 2:
    case 3:
      return d(1)
    case 4:
    case 5:
      return d(2)
    case 6:
    case 7:
    case 8:
      return d(3)
    case 9:
    case 10:
    case 11:
      return d(4)
    case 12:
    case 13:
    case 14:
      return d(5)
    case 15:
    case 16:
      return d(6)
    case 17:
      return d(7)
    case 18:
      return d(8)
    case 19:
      return d(9)
    case 20:
      return d(10)
  }
}

function print (worldName, skillEffect) {
  if (!worldName) {
    return printError("You didn't give me a world name!")
  }

  const world = bahamutSector.find(
    w => w.name.toLowerCase() === worldName.toLowerCase()
  )

  if (!world) {
    return printError(`Sorry, I couldn't find a world named "${worldName}".`)
  }

  let dm = 0
  if (world.population <= 1) dm -= 4
  else if (between(world.population, 6, 7)) dm += 2
  else if (world.population >= 8) dm += 4
  if (world.starport === 'A') dm += 2
  else if (world.starport === 'B') dm += 1
  else if (world.starport === 'E') dm -= 1
  else if (world.starport === 'X') dm -= 3
  if (world.techLevel <= 6) dm -= 1
  else if (world.techLevel >= 9) dm += 2
  if (world.travelCode === 'A') dm -= 2
  else if (world.travelCode === 'R') dm -= 6
  dm += parseInt(skillEffect, 10)

  const numMajor = numLots(Math.max(0, d(2) + dm - 4))
  const numMinor = numLots(Math.max(0, d(2) + dm))
  const numIncidental = numLots(Math.max(0, d(2) + dm + 2))

  const majorLots = collectLots(numMajor, () => d() * 10)
  const minorLots = collectLots(numMinor, () => d() * 5)
  const incidentalLots = collectLots(numIncidental, () => d())

  return [
    {
      embed: {
        color: Color.Green,
        title: World.uwpString(world),
        fields: [
          {
            name: `Major Cargo (${countString(numMajor, 'lot')})`,
            value: majorLots || '—',
            inline: true
          },
          {
            name: `Minor Cargo (${countString(numMinor, 'lot')})`,
            value: minorLots || '—',
            inline: true
          },
          {
            name: `Incidental Cargo (${countString(numIncidental, 'lot')})`,
            value: incidentalLots || '—',
            inline: true
          }
        ]
      }
    }
  ]
}

module.exports = {
  print
}
