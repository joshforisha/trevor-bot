const Color = require('./Color')
const World = require('./World')
const bahamutSector = require('./bahamut-sector.json')
const tradeGoods = require('./trade-goods')
const { classify } = require('./TradeCode')
const { d, isin, sift, takeRandom } = require('./utils')
const { printError, tabularize } = require('./view')

function calculateTrade (
  originUWP,
  originTravelCode,
  destinationUWP,
  destinationTravelCode,
  includeIllegals
) {
  if (originUWP.length !== 9 && originUWP[7] !== '-') {
    return 'Incorrect origin UWP format ("#######-#")'
  }

  if (destinationUWP.length !== 9 && destinationUWP[7] !== '-') {
    return 'Incorrect destination UWP format ("#######-#")'
  }

  const origin = {
    size: parseInt(originUWP[1], 16),
    atmosphere: parseInt(originUWP[2], 16),
    hydrographics: parseInt(originUWP[3], 16),
    population: parseInt(originUWP[4], 16),
    government: parseInt(originUWP[5], 16),
    lawLevel: parseInt(originUWP[6], 16),
    travelCode: originTravelCode || 'Green'
  }
  origin.tradeCodes = classify(origin)

  const destination = {
    size: parseInt(destinationUWP[1], 16),
    atmosphere: parseInt(destinationUWP[2], 16),
    hydrographics: parseInt(destinationUWP[3], 16),
    population: parseInt(destinationUWP[4], 16),
    government: parseInt(destinationUWP[5], 16),
    lawLevel: parseInt(destinationUWP[6], 16),
    travelCode: destinationTravelCode || 'Green'
  }
  destination.tradeCodes = classify(destination)

  const allowedGoods = includeIllegals
    ? tradeGoods
    : tradeGoods.filter(g => g.illegal !== true)

  const [availableGoods, otherGoods] = sift(allowedGoods, g =>
    g.availability(origin)
  )
  takeRandom(d(1), otherGoods).forEach(g => {
    availableGoods.push(g)
  })

  const goods = availableGoods
    .map(g => {
      const purchaseDM = computeDM(g.purchaseDMs, g.saleDMs, origin)
      const saleDM = computeDM(g.saleDMs, g.purchaseDMs, destination)
      const grade = computeGrade(purchaseDM + saleDM)

      return Object.assign({}, g, {
        grade,
        purchaseDM,
        saleDM,
        tons: parseInt(g.tons(), 10)
      })
    })
    .sort((a, b) => (a.type < b.type ? -1 : 1))
    .sort((a, b) => (a.tons > b.tons ? -1 : 1))
    .sort((a, b) => (a.basePrice > b.basePrice ? -1 : 1))
    .sort((a, b) => (a.grade < b.grade ? -1 : 1))
    .map(g =>
      Object.assign({}, g, {
        basePrice: `Cr${g.basePrice.toLocaleString()}`,
        buyDM: g.purchaseDM > -1 ? `+${g.purchaseDM}` : String(g.purchaseDM),
        sellDM: g.saleDM > -1 ? `+${g.saleDM}` : String(g.saleDM),
        tons: g.tons.toString()
      })
    )

  return goods
}

function computeDM (plusDMs, minusDMs, world) {
  let dm = 0

  const pluses = plusDMs
    .filter(
      d =>
        ('tradeCode' in d && isin(d.tradeCode, ...world.tradeCodes)) ||
        ('travelCode' in d &&
          world.travelCode.toLowerCase() === d.travelCode.toLowerCase())
    )
    .map(d => d.dm)
  if (pluses.length > 0) dm += Math.max(...pluses)

  const minuses = minusDMs
    .filter(
      d =>
        ('tradeCode' in d && isin(d.tradeCode, ...world.tradeCodes)) ||
        ('travelCode' in d &&
          world.travelCode.toLowerCase() === d.travelCode.toLowerCase())
    )
    .map(d => d.dm)
  if (minuses.length > 0) dm -= Math.max(...minuses)

  return dm
}

function computeGrade (offset) {
  if (offset > 4) return 'A' // 5+
  if (offset > 1) return 'B' // 2–4
  if (offset > -1) return 'C' // 0–1
  if (offset > -4) return 'D' // -3–-1
  return 'F'
}

function print (args, illegal) {
  if (args.length !== 2) {
    return printError('I need two Bahamut Sector world names!')
  }

  const lowerOrigin = args[0].toLowerCase()
  const origin = bahamutSector.find(w => w.name.toLowerCase() === lowerOrigin)

  if (typeof origin === 'undefined') {
    return printError(
      `Sorry, I can't find a world in the Bahamut Sector named "${args[0]}".`
    )
  }

  const lowerDestination = args[1].toLowerCase()
  const destination = bahamutSector.find(
    w => w.name.toLowerCase() === lowerDestination
  )

  if (typeof destination === 'undefined') {
    return printError(
      `Sorry, I can't find a world in the Bahamut Sector named "${args[1]}".`
    )
  }

  const tradeGoods = calculateTrade(
    origin.uwp,
    origin.travelCode || 'green',
    destination.uwp,
    destination.travelCode || 'green',
    illegal
  )

  if (typeof tradeGoods === 'string') {
    return printError(tradeGoods)
  }

  const goodsTables = tabularize(
    ['Goods', 'Tons', 'Base Price', 'Buy', 'Sell', 'Grade'],
    tradeGoods.map(g => [
      g.type,
      g.tons,
      g.basePrice,
      g.buyDM,
      g.sellDM,
      g.grade
    ]),
    [1, 2]
  )

  return [
    {
      embed: {
        color: Color.Green,
        title: illegal ? 'Black Market' : 'Trade Goods',
        description: [
          `⇠ ${World.uwpString(origin)}`,
          `⇢ ${World.uwpString(destination)}`
        ].join('\n')
      }
    },
    ...goodsTables.map(tbl => ({ content: `\`\`\`${tbl}\`\`\`` }))
  ]
}

module.exports = {
  calculateTrade,
  print
}
