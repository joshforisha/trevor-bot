const Color = require('./Color')
const Contraband = require('./Contraband')
const { printError } = require('./view')
const { rand, takeRandom } = require('./utils')

const {
  Computers,
  Drugs,
  None,
  Psionics,
  Technology,
  Travellers,
  Weapons
} = Contraband

function contraband (government, rnd) {
  switch (government) {
    case 0:
      return [None]
    case 1:
      return [Weapons, Drugs, Travellers]
    case 2:
      return [Drugs]
    case 3:
      return [Technology, Weapons, Travellers]
    case 4:
      return [Drugs, Weapons, Psionics]
    case 5:
      return [Technology, Weapons, Computers]
    case 6:
      return [Weapons, Technology, Travellers]
    case 8:
      return [Drugs, Weapons]
    case 9:
      return [Technology, Weapons, Drugs, Travellers, Psionics]
    case 10:
      return [None]
    case 11:
      return [Weapons, Technology, Computers]
    case 12:
      return [Weapons]
    case 7:
    case 13:
    case 14:
    case 15:
      if (rand(1, 6, rnd) >= 5) return [None]
      return takeRandom(rand(0, 3, rnd), [
        Computers,
        Drugs,
        Psionics,
        Technology,
        Travellers,
        Weapons
      ])
  }
}

function description (government) {
  switch (government) {
    case 0:
      return 'No government structure. In many cases, family bonds predominate.'
    case 1:
      return 'Ruling functions are assumed by a company managerial elite, and most citizenry are company employees or dependants.'
    case 2:
      return 'Ruling functions are reached by the advice and consent of the citizenry directly.'
    case 3:
      return 'Ruling functions are performed by a restricted minority, with little or no input from the mass of citizenry.'
    case 4:
      return 'Ruling functions are performed by elected representatives.'
    case 5:
      return 'Ruling functions are performed by specific individuals for persons who agree to be ruled by them. Relationships are based on the performance of technical activities which are mutually beneficial.'
    case 6:
      return 'Ruling functions are performed by an imposed leadership answerable to an outside group.'
    case 7:
      return 'No central authority exists; rival governments compete for control. Law level refers to the government nearest the starport.'
    case 8:
      return 'Ruling functions are performed by government agencies employing individuals selected for their expertise.'
    case 9:
      return 'Ruling functions are performed by agencies which have become insulated from the governed citizens.'
    case 10:
      return 'Ruling functions are performed by agencies directed by a single leader who enjoys the overwhelming confidence of the citizens.'
    case 11:
      return 'A previous charismatic dictator has been replaced by a leader through normal channels.'
    case 12:
      return 'Ruling functions are performed by a select group of members of an organisation or class which enjoys the overwhelming confidence of the citizenry.'
    case 13:
      return 'Ruling functions are performed by a religious organisation without regard to the specific needs of the citizenry.'
    case 14:
      return 'Government by a single religious leader having absolute power over the citizenry.'
    case 15:
      return 'Government by an all-powerful minority which maintains absolute control through widespread coercion and oppression.'
  }
}

function name (government) {
  switch (government) {
    case 0:
      return 'None'
    case 1:
      return 'Company/Corporation'
    case 2:
      return 'Participating Democracy'
    case 3:
      return 'Self-Perpetuating Oligarchy'
    case 4:
      return 'Representative Democracy'
    case 5:
      return 'Feudal Technocracy'
    case 6:
      return 'Captive Government'
    case 7:
      return 'Balkanisation'
    case 8:
      return 'Civil Service Bureaucracy'
    case 9:
      return 'Impersonal Bureaucracy'
    case 10:
      return 'Charismatic Dictator'
    case 11:
      return 'Non-Charismatic Leader'
    case 12:
      return 'Charismatic Oligarchy'
    case 13:
      return 'Religious Dictatorship'
    case 14:
      return 'Religious Autocracy'
    case 15:
      return 'Totalitarian Oligarchy'
  }
}

function print (government) {
  if (typeof government !== 'string') {
    return printError('You didn\'t give me a *government code* to lookup!')
  }

  const rating = parseInt(government, 16)
  const govName = name(government)

  if (!govName) {
    return printError(
      `Sorry, I couldn't find a government coded as "${government}".`
    )
  }

  return [
    {
      embed: {
        color: Color.Green,
        title: `Government **${rating}**: ${name}`,
        description: description(rating)
      }
    }
  ]
}

module.exports = {
  contraband,
  description,
  name,
  print
}
