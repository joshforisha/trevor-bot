function description (techLevel) {
  switch (techLevel) {
    case 0:
      return 'Primitive; Stone Age'
    case 1:
      return 'Primitive; Bronze/Iron Age'
    case 2:
      return 'Primitive; Renaissance technology'
    case 3:
      return 'Primitive; Colonisation Age'
    case 4:
      return 'Early Industrial; early 20th century'
    case 5:
      return 'Middle Industrial; mid 20th century'
    case 6:
      return 'Late Industrial; late 20th century'
    case 7:
      return 'Pre-Stellar; early Space Age'
    case 8:
      return 'Pre-Stellar; fusion technology'
    case 9:
      return 'Pre-Stellar; gravity manipulation, planetary colonisation'
    case 10:
      return 'Early Stellar; orbital technology'
    case 11:
      return 'Early Stellar; AI prevalence'
    case 12:
      return 'Average Stellar; weather control, advanced weaponry'
    case 13:
      return 'Average Stellar; battle dress, cloning, advanced spacecraft'
    case 14:
      return 'Average Stellar; fusion weapons, flying cities'
    case 15:
      return 'High Stellar; vastly increased lifespans'
  }
}

module.exports = {
  description
}
