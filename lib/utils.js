function between (x, a, b) {
  return x >= a && x <= b
}

function con (x, a, b) {
  return Math.max(a, Math.min(b, x))
}

function countString (num, single, plural) {
  if (num === 1) return `${num} ${single}`
  if (!plural) plural = `${single}s`
  return `${num} ${plural}`
}

function d (num = 1, rnd = Math.random) {
  let total = 0
  for (let i = 0; i < num; i++) total += rand(1, 6, rnd)
  return total
}

function d3 (num = 1, rnd = Math.random) {
  let total = 0
  for (let i = 0; i < num; i++) total += rand(1, 3, rnd)
  return total
}

function draw (xs, rnd = Math.random) {
  if (xs.length > 0) {
    return xs[rand(0, xs.length - 1, rnd)]
  }
}

function initialize (length, fill = null) {
  const arr = []
  while (arr.length < length) {
    if (typeof fill === 'function') arr.push(fill())
    else arr.push(fill)
  }
  return arr
}

function isin (x, ...xs) {
  return xs.indexOf(x) > -1
}

function lines (...items) {
  return items.filter(x => x !== null && x !== '').join('\n')
}

function padLeft (string, length, char = ' ') {
  let str = `${string}`
  while (str.length < length) str = `${char}${str}`
  return str
}

function padRight (string, length, char = ' ') {
  let str = `${string}`
  while (str.length < length) str = `${str}${char}`
  return str
}

function rand (min, max, rnd = Math.random) {
  return Math.floor(rnd() * (max - min + 1) + min)
}

function randq (q, min, max, rnd = Math.random) {
  return Math.floor(min + (max - min + 1) * Math.pow(rnd(), q))
}

function sift (xs, pred) {
  const pass = []
  const fail = []
  xs.forEach(x => {
    if (pred(x)) pass.push(x)
    else fail.push(x)
  })
  return [pass, fail]
}

function takeRandom (count, xs, rnd = Math.random) {
  const shuffledItems = []
  const sourceItems = xs.concat()
  while (sourceItems.length > 0 && shuffledItems.length < count) {
    const i = rand(0, sourceItems.length - 1, rnd)
    shuffledItems.push(sourceItems[i])
    sourceItems.splice(i, 1)
  }
  return shuffledItems
}

function unit (x, u) {
  let prefix = ''
  let y = x
  if (x >= 1e9) {
    prefix = 'G'
    y = Math.floor(x / 1e9)
  } else if (x >= 1e6) {
    prefix = 'M'
    y = Math.floor(x / 1e6)
  } else if (x >= 1e3) {
    prefix = 'k'
    y = Math.floor(x / 1e3)
  }

  return `${y.toLocaleString()} ${prefix}${u}`
}

module.exports = {
  between,
  con,
  countString,
  d,
  d3,
  draw,
  initialize,
  isin,
  lines,
  padLeft,
  padRight,
  rand,
  randq,
  sift,
  takeRandom,
  unit
}
