const Atmosphere = require('./Atmosphere')
const Color = require('./Color')
const Government = require('./Government')
const Hydrographics = require('./Hydrographics')
const LawLevel = require('./LawLevel')
const Population = require('./Population')
const Size = require('./Size')
const Spaceport = require('./Spaceport')
const TechLevel = require('./TechLevel')
const TradeCode = require('./TradeCode')
const bahamutSector = require('./bahamut-sector.json')
const seedrandom = require('seedrandom')
const { emsp, letters, printError } = require('./view')
const { lines, padLeft } = require('./utils')

function basesText (bases) {
  return bases
    .split(' ')
    .map(b => {
      switch (b) {
        case 'R':
          return 'Research'
        case 'N':
          return 'Naval'
        case 'S':
          return 'Scout'
        case 'T':
          return 'TAS'
        default:
          return ''
      }
    })
    .join(', ')
}

function printInfo (inputName) {
  if (typeof inputName !== 'string') {
    return printError('You didn\'t give me a *world name* to lookup!')
  }

  const world = bahamutSector.find(
    w => w.name.toLowerCase() === inputName.toLowerCase()
  )

  if (typeof world === 'undefined') {
    return printError(
      `Sorry, I can't find a world in the Bahamut Sector named "${inputName}".`
    )
  }

  const {
    atmosphere,
    government,
    hydrographics,
    lawLevel,
    population,
    size,
    starport,
    techLevel
  } = properties(world)

  const seed = padLeft(world.hex.toString(), 4, '0')
  const rnd = seedrandom(seed)

  const berthingCost =
    'berthingCost' in world
      ? world.berthingCost
      : Spaceport.generateBerthingCost(starport, rnd)

  const diameter =
    'diameter' in world ? world.diameter : Size.generateDiameter(size, rnd)

  const hydroPercentage = Hydrographics.generatePercentage(hydrographics, rnd)

  const populationNumber =
    'population' in world
      ? world.population
      : Population.generateNumber(population, rnd)

  const rotationPeriod =
    'rotationPeriod' in world
      ? world.rotationPeriod
      : Size.generateRotationPeriod(size, rnd)

  const temperature =
    'temperature' in world
      ? world.temperature
      : Atmosphere.generateTemperature(atmosphere, rnd)

  return [
    {
      embed: {
        color: Color.Green,
        title: uwpString(world),
        fields: [
          {
            name: `Starport: **Class ${starport}** *${Spaceport.quality(
              starport
            )}*`,
            value: lines(
              Spaceport.fuel(starport),
              `Berthing Cost: ${berthingCost}`,
              `Bases: ${basesText(world.bases) || '*None*'}`,
              `Facilities: ${Spaceport.facilities(starport)}`
            )
          },
          {
            name: `Size: **${size}** *${Size.name(size)}*`,
            value: lines(
              Size.gravity(size),
              `Diameter: ${diameter}`,
              `Rotation Period: ${rotationPeriod}`
            )
          },
          {
            name: `Atmosphere: **${atmosphere}** *${Atmosphere.name(
              atmosphere
            )}*`,
            value: lines(
              Atmosphere.requirement(atmosphere),
              `Temperature: ${temperature}`
            )
          },
          {
            name: `Hydrographics: **${hydrographics}**`,
            value: `${hydroPercentage} (${Hydrographics.description(
              hydrographics
            )})`
          },
          {
            name: `Population: **${population}**`,
            value: `${populationNumber} people`
          },
          {
            name: `Government: **${government}** *${Government.name(
              government
            )}*`,
            value: Government.description(government)
          },
          {
            name: `Law Level: **${lawLevel}**`,
            value: `Banned: ${LawLevel.bannedItems(lawLevel)}`
          },
          {
            name: `Tech Level: **${techLevel}**`,
            value: TechLevel.description(techLevel)
          },
          {
            name: 'Trade Codes',
            value:
              world.tradeCodes
                .split(' ')
                .map(TradeCode.name)
                .join(', ') || '*None*'
          }
        ]
      }
    }
  ]
}

function properties ({ uwp }) {
  return {
    starport: uwp[0],
    size: parseInt(uwp[1], 16),
    atmosphere: parseInt(uwp[2], 16),
    hydrographics: parseInt(uwp[3], 16),
    population: parseInt(uwp[4], 16),
    government: parseInt(uwp[5], 16),
    lawLevel: parseInt(uwp[6], 16),
    techLevel: parseInt(uwp[8], 16)
  }
}

function uwpArray ({
  allegiance,
  bases,
  hex,
  name,
  subsector,
  tradeCodes,
  travelCode,
  uwp
}) {
  const vals = [
    name,
    `${letters[subsector]}-${padLeft(hex.toString(), 4, '0')}`,
    uwp,
    bases,
    tradeCodes
  ]

  if (travelCode === 'Amber') vals.push('A')
  else if (travelCode === 'Red') vals.push('R')
  else vals.push('')

  if (allegiance) vals.push(allegiance)
  else vals.push('')

  return vals
}

function uwpString (world) {
  return uwpArray(world)
    .filter(x => x !== '')
    .join(emsp)
}

module.exports = {
  printInfo,
  properties,
  uwpArray,
  uwpString
}
