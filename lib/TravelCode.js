const TravelCode = {
  Amber: 'Amber',
  Green: 'Green',
  Red: 'Red'
}

module.exports = TravelCode
